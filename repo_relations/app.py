import logging

import boto3
from flask import Flask

import click  # noqa

from repo_relations import settings

# from repo_relations.utils.scm import get_api as get_scm_api

static_folder = settings.get('STATIC_FOLDER')

app = Flask(__name__, static_folder=static_folder)
app.config.from_object(settings)
app.debug = app.config['DEBUG']

if not app.debug:
    boto3.set_stream_logger(level=logging.CRITICAL)  # pragma no cover
    logging.getLogger('botocore').setLevel(logging.CRITICAL)  # pragma no cover
    app.logger.setLevel(logging.INFO)  # pragma no cover
else:
    app.logger.setLevel(logging.DEBUG)  # pragma no cover


# Commands
# @app.cli.command()
# @click.option('--scm', help='Source control manager type ie: stash, gitlab ect')
# def seed(scm):
#     scm_api = get_scm_api(scm)
#     scm_projects = scm_api.projects(refresh=True)

#     for scm_project in scm_projects:
#         project_repos = scm_api.repos(scm_project.key, refresh=True)


if __name__ == '__main__':
    app.run(DEBUG=True)  # pragma no cover
