import logging
from os import getenv

logger = logging.getLogger('repo_relations')


class SettingsError(Exception):
    pass


def bool_env(var_name, default=False):
    """
    Attempt to coerce bool value from app environment
    """
    test_val = getenv(var_name, default)

    if test_val in ('False', 'false', '0'):
        return False
    else:
        return bool(test_val)


def float_env(var_name, default=0.0):
    """
    Attempt to coerce float value from app environment
    """
    return float(getenv(var_name, default))


def int_env(var_name, default=0):
    """
    Attempt to coerce int value from app environment
    """
    return int(getenv(var_name, default))


def str_env(var_name, default=''):
    """
    Attempt to coerce str value from app environment
    """
    return str(getenv(var_name, default))


def str_list_env(var_name, default=[]):
    """Attempt to coerce list<str> value from app environment
    """
    vars = getenv(var_name, default)

    return list([v.strip() for v in vars.split(',')])


# Settings

# Run in debug mode? Never do this in prod!
DEBUG = bool_env('DEBUG', True)

# WSGI Host configuration
HOST = str_env('HOST', '127.0.0.1')

# WSGI Port configuration
PORT = int_env('PORT', 8080)

# Flask Static Assets Folder
STATIC_FOLDER = str_env('STATIC_FOLDER', 'public')

# SCM GitLab
SCM_GITLAB_URL = str_env('SCM_GITLAB_URL', '')
SCM_GITLAB_USERNAME = str_env('SCM_GITLAB_USERNAME', '')
SCM_GITLAB_PASSWORD = str_env('SCM_GITLAB_PASSWORD', '')

# SCM Stash
SCM_STASH_URL = str_env('SCM_STASH_URL', '')
SCM_STASH_USERNAME = str_env('SCM_STASH_USERNAME', '')
SCM_STASH_PASSWORD = str_env('SCM_STASH_PASSWORD', '')

# DB General
DB_BACKEND = str_env('DB_BACKEND', 'neo4j')

# DB
DB_HOST = str_env('DB_HOST', '')
DB_USERNAME = str_env('DB_USERNAME', '')
DB_PASSWORD = str_env('DB_PASSWORD', '')

# Git
GIT_TEMP_PATH = str_env("GIT_TEMP_PATH", '/tmp')

# Projects
PROJECTS_INCLUDE = str_list_env('PROJECTS_INCLUDE', '')
PROJECTS_IGNORE = str_list_env('PROJECTS_IGNORE', '')
REPOS_IGNORE = str_list_env('REPOS_IGNORE', '')

# Settings utility methods


def get(name, default=None):
    """
    Get value from settings module scope
    """
    logger.debug('settings.get({}, {})'.format(name, default))
    return globals().get(name, default)


def __set(name, default=None):  # pragma: no cover
    """
    Set value in settings module scope
    """
    logging.debug('settings.__set({}, {})'.format(name, default))
    return globals().set(name, default)
