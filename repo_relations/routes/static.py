from flask import jsonify

from repo_relations.app import app


@app.route('/')
def index():
    """
    Blank for now index page
    """
    return '', 200


@app.route('/health')
def health():
    """
    Always healthy!
    Until we're not...
    """
    response = jsonify({
        'status': 'OK'
    })

    return response, 200


@app.errorhandler(404)
def page_not_found(e):
    """
    Must have lost the binary stream somewhere!
    """
    response = jsonify({
        'status': 'Not Found'
    })

    return response, 404


@app.errorhandler(500)
def internal_error(e):
    """
    Something gummed up the works!
    """
    response = jsonify({
        'status': 'Not Working'
    })

    return response, 500
