from pprint import pprint  # noqa
from flask import jsonify, request

from repo_relations.app import app
from repo_relations.models import get_model

from repo_relations.utils.scm import get_api

from repo_relations.routes.v1.utils import (
    normalize_repos,
    normalize_projects
)

MODEL = get_model()


@app.route('/v1/scm/<service>/projects', methods=['GET'])
def scm_projects(service):
    """Endpoint for collecting git project listing from a SCM service

    Arguments:
        service {str} -- Server name ie: gitlab, stash ect...
    """

    refresh = 'refresh' in request.args

    api = get_api(service)
    projects = api.projects(refresh)

    response = jsonify({
        'status': 'OK',
        'projects': normalize_projects(projects)
    })

    return response, 200


@app.route('/v1/scm/<service>/repos', methods=['GET'])
def scm_repos(service):
    """Endpoint for collecting git repository listing from a SCM service

    Arguments:
        service {str} -- Server name ie: gitlab, stash ect...
    """

    refresh = 'refresh' in request.args

    api = get_api(service)
    repos = api.repos(refresh)

    # Flatten repos for all projects from api result
    repo_list = {}
    repo_counter = 0
    for project_name, repo_data in repos.items():
        if project_name not in repo_list:
            repo_list[project_name] = normalize_repos(repo_data['repos'])
            repo_counter += len(repo_list[project_name])

    response = jsonify({
        'status': 'OK',
        'count': repo_counter,
        'repositories': repo_list
    })

    return response, 200


@app.route('/v1/scm/<service>/<project>/repos', methods=['GET'])
def scm_project_repos(service, project):
    """Endpoint for collecting git repository listing from a SCM service

    Arguments:
        service {str} -- Server name ie: gitlab, stash ect...
        project {str} -- Name of the project to collect repositories from
    """

    refresh = 'refresh' in request.args

    api = get_api(service)
    repos = api.project_repos(project, refresh)

    response = jsonify({
        'status': 'OK',
        'repositories': normalize_repos(repos)
    })

    return response, 200


@app.route('/v1/scm/<service>/<project>/repo/<repo_name>', methods=['GET'])
def scm_project_repo(service, project, repo_name):
    """Endpoint for fetching git repository metadata from a SCM service

    Arguments:
        service {str} -- Server name ie: gitlab, stash ect...
        project {str} -- Name of the project to collect repositories from
        repo_name {str} -- Name of the repository to fetch
    """

    refresh = 'refresh' in request.args

    api = get_api(service)
    repos = api.project_repos(project=project, refresh=refresh)

    response = jsonify({
        'status': 'OK',
        'repositories': normalize_repos(repos, repo_name)
    })

    return response, 200
