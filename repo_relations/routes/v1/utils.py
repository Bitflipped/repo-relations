from repo_relations.utils.scm import get_api


def normalize_projects(raw_projects):
    """Utility method for converting list of project models to
    simple python dict for JSON compatability.

    Arguments:
        raw_projects {[type]} -- [description]
    """

    # Dirty hack to normalize things for now
    project_response = []
    for project in raw_projects:
        project_response.append({
            'name': project.name,
            'description': project.description,
            'key': project.key
        })

    return project_response


def normalize_repos(raw_repos, repo_name=None):
    """Utility method for converting list of repo models to
    simple python dict for JSON compatability. If repo_name
    given then we return a single dict for that repo.

    Arguments:
        raw_repos {list<Repo>} -- List of Repo models to normalize
        repo_name {str} -- Name of single repo to extract from listing

    Keyword Arguments:
        repo_name {[type]} -- [description] (default: {None})
    """

    # Dirty hack to normalize things for now
    repos = []
    for repo in raw_repos:
        repo_temp = {
            'name': repo.name,
            'description': repo.description,
            'path': repo.path,
            'url': repo.url,
            'ssh_clone_url': repo.ssh_clone_url,
            'http_clone_url': repo.http_clone_url
        }

        if repo_name is not None and repo_name.lower() == repo.name.lower():
            return [repo_temp]
        else:
            repos.append(repo_temp)

    return repos


def get_repo_data(service, project, repo_name):
    """Utility method for using the SCM backend to get metadata for
    requested repo_name

    Arguments:
        repo_name {str} -- Name of single repo to extract from listing

    Keyword Arguments:
        repo_name {[type]} -- [description] (default: {None})
    """
    api = get_api(service)

    # Dirty hack to normalize things for now
    repos = api.repos(project)
    for repo in repos:
        if repo_name.lower() == repo.name.lower():
            return [
                {
                    'name': repo.name,
                    'description': repo.description,
                    'path': repo.path,
                    'url': repo.url,
                    'ssh_clone_url': repo.ssh_clone_url,
                    'http_clone_url': repo.http_clone_url
                }
            ]

    raise(Exception)
