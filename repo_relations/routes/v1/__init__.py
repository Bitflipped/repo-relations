from repo_relations.routes.v1 import scm  # noqa
from repo_relations.routes.v1 import git  # noqa
from repo_relations.routes.v1 import deps  # noqa
from repo_relations.routes.v1 import relations  # noqa
from repo_relations.routes.v1 import manage  # noqa
