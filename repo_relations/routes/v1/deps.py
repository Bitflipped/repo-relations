from flask import jsonify, request

from pprint import pprint  # noqa

from repo_relations.app import app
from repo_relations.utils.git import git_repo as lib_git_repo
from repo_relations.utils.languages.utils import get_api

from repo_relations.routes.v1.utils import (
    get_repo_data
)


@app.route('/v1/deps/<service>/<project>/<repo_name>', methods=['GET'])
def deps_fetch(service, project, repo_name):
    """Endpoint for fetching git repository from a SCM service

    Arguments:
        service {str} -- Server name ie: gitlab, stash ect...
        project {str} -- Name of the project to collect repositories from
        repo_name {str} -- Name of the repository to fetch
    """

    repo = get_repo_data(service, project, repo_name)
    clone_type = request.args.get('clone_type', 'ssh')
    clone_url = repo[0]['{}_clone_url'.format(clone_type)] if \
        clone_type in ['ssh', 'http'] else repo[0]['ssh_clone_url']

    git_repo_mgr = lib_git_repo.GitRepo(project, clone_url)
    git_repo_path, git_repo_size = git_repo_mgr.clone()

    deps = get_api(git_repo_path)

    deps_response = {}
    for d in [dep for dep in deps if len(dep.dep_files) > 0]:
        dep_dicts = list(d.get_all())

        for dep_dict in dep_dicts:
            if dep_dict is not None:
                if d.LANGUAGE not in deps_response:
                    deps_response[d.LANGUAGE] = {}
                deps_response[d.LANGUAGE].update(dep_dict)

    response = jsonify({
        'status': 'OK',
        'deps': deps_response
    })

    return response, 200
