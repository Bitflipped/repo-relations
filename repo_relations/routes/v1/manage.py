from pprint import pprint  # noqa
from flask import jsonify, request

import neomodel

from repo_relations.app import app
from repo_relations.models import get_model

from repo_relations.utils.scm import get_api
from repo_relations.utils.languages.utils import get_api as get_language_api
from repo_relations.utils.git import git_repo as lib_git_repo

MODEL = get_model()


@app.route('/v1/manage/<service>/seed', methods=['GET'])
def db_seed(service):
    clone_type = request.args.get('clone_type', 'ssh')
    scm_api = get_api(service)

    neomodel.clear_neo4j_database(neomodel.db)

    app.logger.info(f'Phase 1 starting: Discovering project, repos and dependencies')
    projects = scm_api.projects(refresh=True)
    dependency_nodes = []
    for project in projects:
        if len(app.config['PROJECTS_INCLUDE']) > 1 and f'{project.key}' not in app.config['PROJECTS_INCLUDE']:
                app.logger.info(f'Skipping project {project.key} per include rules')
                continue

        app.logger.debug(f'Processing project {project.name}')
        repos = scm_api.project_repos(project.key, refresh=True)

        for repo in repos:
            if f'{project.key}/{repo.name}' in app.config['REPOS_IGNORE']:
                app.logger.info(f'Skipping repo {project.key}/{repo.name} per ignore rules')
                continue

            app.logger.debug(f'Linking {repo.name} to {project.name}')
            repo.project.connect(project)

            # Process dependencies
            clone_url = getattr(repo, f'{clone_type}_clone_url') if \
                clone_type in ['ssh', 'http'] else repo.ssh_clone_url

            git_repo_mgr = lib_git_repo.GitRepo(project.key, clone_url)
            git_repo_path, git_repo_size = git_repo_mgr.clone()

            deps = get_language_api(git_repo_path)
            for d in [dep for dep in deps if len(dep.dep_files) > 0]:
                dep_models = list(d.get_all())

                for dependency in dep_models:
                    if dependency is not None:
                        app.logger.debug(f'### DepModel {dependency}')
                        repo.dependencies.connect(dependency)
                        dependency_nodes.append(dependency)

    # Reiterate and link up dependencies
    app.logger.info(f'Phase 2 starting: linking cached deps to related nodes')
    for dependency_node in dependency_nodes:
        if dependency_node is not None:
            app.logger.debug(f'Linking up dependencies for {dependency_node.name}')
            dependency_node.link_cached()

    response = jsonify({
        'status': 'OK'
    })

    return response, 200
