from flask import jsonify, request

from repo_relations.app import app
from repo_relations.utils.git import git_repo as lib_git_repo

from repo_relations.routes.v1.utils import (
    get_repo_data
)


@app.route('/v1/git/<service>/<project>/<repo_name>', methods=['GET'])
def git_repo(service, project, repo_name):
    """Endpoint for fetching git repository from a SCM service

    Arguments:
        service {str} -- Server name ie: gitlab, stash ect...
        project {str} -- Name of the project to collect repositories from
        repo_name {str} -- Name of the repository to fetch
    """

    repo = get_repo_data(service, project, repo_name)
    clone_type = request.args.get('clone_type', 'ssh')
    clone_url = repo[0]['{}_clone_url'.format(clone_type)] if \
        clone_type in ['ssh', 'http'] else repo[0]['ssh_clone_url']

    git_repo_mgr = lib_git_repo.GitRepo(project, clone_url)
    git_repo_path, git_repo_size = git_repo_mgr.clone()

    response = jsonify({
        'status': 'OK',
        'clone_url': clone_url,
        'local_path': git_repo_path,
        'local_repo_size': git_repo_size
    })

    return response, 200
