from flask import jsonify

from pprint import pprint  # noqa

from repo_relations.models import get_model
from repo_relations.app import app

MODEL = get_model()


@app.route('/v1/relations/dependent/<project>/<repo_name>', methods=['GET'])
def dependent_repo(project, repo_name):
    """Endpoint for fetching 1st order repos that depend on given project/repo

    Arguments:
        project {str} -- Project key
        repo_name {str} -- Name of the repository to fetch
    """

    # TODO Refactor to not lean heavily on neomodel concepts
    app.logger.debug(f'Searching for project {project}')
    project_model = MODEL.Project.nodes.get_or_none(key=project)
    if project_model is None:
        app.logger.debug(f'Project {project}, not found in models')
        response = jsonify({
            'status': 'NOT FOUND',
            'message': f'Project {project} not found.'
        })

        return response, 404

    app.logger.debug(f'Searching {project_model.name} for {repo_name}')
    repo_models = project_model.repos.search(name=repo_name)
    if repo_models is None or len(repo_models) == 0:
        app.logger.debug(f'Repo {repo_name}, not found in {project_model.name}')
        response = jsonify({
            'status': 'NOT FOUND',
            'message': f'Project {repo_name} not found in {project}.'
        })

        return response, 404
    elif len(repo_models) == 1:
        repo_model = repo_models[0]
    else:
        app.logger.debug(f'Repo {repo_name}, found in {project_model.name} multiple times.')
        response = jsonify({
            'status': 'NOT FOUND',
            'message': f'Project {repo_name} found in {project} multiple times.'
        })

        return response, 400

    dependent_repos = []
    for dependency in repo_model.dependencies:
        print('################')
        dependent_repos += dependency.get_dependent_dependencies()

    response = jsonify({
        'status': 'OK',
        'dependent_repos': dependent_repos
    })

    return response, 200
