import unittest
import os

from repo_relations import settings

env_settings = [
    ('DEBUG', 'true')
]


class SettingsTest(unittest.TestCase):
    def setUp(self):
        for env_setting in env_settings:
            os.environ[env_setting[0]] = os.getenv(env_setting[0], env_setting[1])

    def test_get(self):
        self.assertEqual(
            settings.get('DEBUG', False),
            True
        )
