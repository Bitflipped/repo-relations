from repo_relations.app import app

from repo_relations.utils.scm.stash.api import StashAPI
from repo_relations.utils.scm.gitlab.api import GitLabAPI


def get_api(service):
    """Return API object based on service passed in.

    Arguments:
        service {str} -- Name of service to return api wrapper for.

    Returns:
        [type] -- [description]
    """

    if service == 'stash':
        url = app.config['SCM_STASH_URL']
        username = app.config['SCM_STASH_USERNAME']
        password = app.config['SCM_STASH_PASSWORD']

        return StashAPI(url, username, password)

    elif service == 'gitlab':
        url = app.config['SCM_GITLAB_URL']
        username = app.config['SCM_GITLAB_USERNAME']
        password = app.config['SCM_GITLAB_PASSWORD']

        return GitLabAPI(url, username, password)
