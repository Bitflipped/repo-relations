import gitlab

from datetime import datetime
from pprint import pprint  # noqa

from repo_relations.app import app
from repo_relations.models import get_model

MODEL = get_model()


class GitLabError(Exception):
    pass


class GitLabAPI(object):
    """API Wrapper for GitLab SCM
    Used as an interface to normalize interations and support various
    SCM services through a common interface!
    """

    projects_cache_timestamp = None
    repository_cache_timestamp = None

    projects_cache = None
    repository_cache = {}

    def __init__(self, url, username, password):
        """Wrapper Initializer

        Arguments:
            url {str} -- URL for the SCM service.
            username {str} -- Username userd for logging in to SCM service.
            password {str} -- Password used for logging in to SCM service.
        """

        try:
            self.connection = gitlab.GitLab(url, username, password)
            self.connection.auth()
        except Exception as e:
            raise(GitLabError(e))

    def projects(self, refresh=False):
        """Returns listing of SCM projects

        Arguments:
            refresh {bool} -- If True force refresh of the cache

        Returns:
            list() -- List or projects
        """

        # If forced refresh or first init or expired then we fetch
        if (refresh or GitLabAPI.projects_cache_timestamp is None) or \
           (
               datetime.now() - GitLabAPI.projects_cache_timestamp
           ).seconds > 3600:
            app.logger.debug('Fetching project listing from GitLab...')

            try:
                p = self.connection.groups.list()
            except Exception as e:
                raise(GitLabError(e))

            projects = {}
            for project in p:
                if (len(app.config['PROJECTS_INCLUDE']) > 1 and project.name not in app.config['PROJECTS_INCLUDE']):
                    app.logger.info(f'Skipping project {project.name} per project include rules')
                    continue
                elif project.nmae in app.config['PROJECTS_IGNORE']:
                    app.logger.info(f'Ignoring project {project.name} per project ignore config')
                    continue
                ptmp = MODEL.Project(
                    name=project.name,
                    description=project['description'] if 'description' in project else '',
                    key=project.path
                ).save()

                projects[project.path] = ptmp

            GitLabAPI.projects_cache = projects
            GitLabAPI.projects_cache_timestamp = datetime.now()

        return GitLabAPI.projects_cache.values()

    def repos(self, refresh=False):
        """Returns listing of SCM all repositories

        Arguments:
            refresh {bool} -- If True force refresh of the cache

        Returns:
            list() -- Listing of repositories
        """

        # If forced refresh or first init or expired then we fetch
        if (refresh or GitLabAPI.repository_cache_timestamp is None) or \
           (
               datetime.now() - GitLabAPI.repository_cache_timestamp
                ).seconds > 3600:
            app.logger.debug(
                'Fetching full repo listing from GitLab...'
            )
            GitLabAPI.repository_cache = {}

            try:
                raw_projects = self.connection.projects.list()
            except Exception as e:
                raise(GitLabError(e))

            for project in raw_projects:
                rtmp = MODEL.Repo(
                        name=project.name,
                        description=project.description,
                        path=project.path_with_namespace,
                        url=project.web_url,
                        ssh_clone_url=project.ssh_url_to_repo,
                        http_clone_url=project.http_url_to_repo
                    ).save()

                group = project.path

                # If project was specified we update that
                if group is not None:
                    if project not in GitLabAPI.repository_cache:
                        GitLabAPI.repository_cache[group] = {}
                        GitLabAPI.repository_cache[group]['repos'] = []
                        GitLabAPI.repository_cache[group]['cache_timestamp'] \
                            = datetime.now()

                if rtmp in GitLabAPI.repository_cache[group]['repos']:
                    app.logger.warn(f'Duplicate entry {rtmp.name} found in response from GitLab for group {group}')

                GitLabAPI.repository_cache[group]['repos'].append(rtmp)

            GitLabAPI.repository_cache_timestamp = datetime.now()

        return GitLabAPI.repository_cache

    def project_repos(self, project, refresh=False):
        """Returns listing of SCM repositories within a project

        Arguments:
            project {str} -- Name of the project
            refresh {bool} -- If True force refresh of the cache

        Returns:
            list() -- List or repositories
        """

        # If forced refresh or first init or expired then we fetch
        if refresh or \
            project not in GitLabAPI.repository_cache or \
            (datetime.now() - GitLabAPI.repository_cache[project]
                ['cache_timestamp']).seconds > 3600:

            app.logger.debug(
                'Fetching {} repo listing from GitLab...'.format(project)
            )

            try:
                raw_repos = self.connection.projects[project.upper()].repos.list()
            except Exception as e:
                raise(GitLabError(e))

            repos = []
            for repo in raw_repos:
                links = {}
                for link in repo['links']['clone']:
                    links[link['name']] = link['href']

                rtmp = MODEL.Repo(
                        name=repo['name'],
                        description=repo['description'] if 'description' in repo else '',
                        path=repo['slug'] if 'slug' in repo else '',
                        url=repo['links']['self'][0]['href'],
                        ssh_clone_url=links['ssh'],
                        http_clone_url=links['http']
                    ).save()

                repos.append(rtmp)

            # If project was specified we update that
            if project is not None:
                if project not in GitLabAPI.repository_cache:
                    GitLabAPI.repository_cache[project] = {}

                GitLabAPI.repository_cache[project]['repos'] = repos
                GitLabAPI.repository_cache[project]['cache_timestamp'] \
                    = datetime.now()

        return GitLabAPI.repository_cache[project]['repos']
