import stashy
from stashy.errors import (
    NotFoundException
)

from datetime import datetime
from pprint import pprint  # noqa

from repo_relations.app import app
from repo_relations.models import get_model

MODEL = get_model()


class StashError(Exception):
    pass


class StashAPI(object):
    """API Wrapper for Stash SCM
    Used as an interface to normalize interations and support various
    SCM services through a common interface!
    """

    projects_cache_timestamp = None
    repository_cache_timestamp = None

    projects_cache = None
    repository_cache = {}

    def __init__(self, url, username, password):
        """Wrapper Initializer

        Arguments:
            url {str} -- URL for the SCM service.
            username {str} -- Username userd for logging in to SCM service.
            password {str} -- Password used for logging in to SCM service.
        """

        try:
            self.connection = stashy.connect(url, username, password)
        except Exception as e:
            raise(StashError(e))

    def projects(self, refresh=False):
        """Returns listing of SCM projects

        Arguments:
            refresh {bool} -- If True force refresh of the cache

        Returns:
            list() -- List or projects
        """

        # If forced refresh or first init or expired then we fetch
        if (refresh or StashAPI.projects_cache_timestamp is None) or \
           (
               datetime.now() - StashAPI.projects_cache_timestamp
           ).seconds > 3600:
            app.logger.debug('Fetching project listing from stash...')

            try:
                p = self.connection.projects.list()
            except NotFoundException as e:
                raise(StashError(e))

            projects = {}
            for project in p:
                if (len(app.config['PROJECTS_INCLUDE']) > 1 and project['key'] not in app.config['PROJECTS_INCLUDE']):
                    app.logger.info(f'Skipping project {project["key"]} per project include rules')
                    continue
                elif project['key'] in app.config['PROJECTS_IGNORE']:
                    app.logger.info(f'Ignoring project {project["key"]} per project ignore config')
                    continue
                ptmp = MODEL.Project(
                    name=project['name'],
                    description=project['description'] if 'description' in project else '',
                    key=project['key']
                ).save()

                projects[project['key']] = ptmp

            StashAPI.projects_cache = projects
            StashAPI.projects_cache_timestamp = datetime.now()

        return StashAPI.projects_cache.values()

    def repos(self, refresh=False):
        """Returns listing of SCM all repositories

        Arguments:
            refresh {bool} -- If True force refresh of the cache

        Returns:
            list() -- Listing of repositories
        """

        # If forced refresh or first init or expired then we fetch
        if (refresh or StashAPI.repository_cache_timestamp is None) or \
           (
               datetime.now() - StashAPI.repository_cache_timestamp
                ).seconds > 3600:
            app.logger.debug(
                'Fetching full repo listing from stash...'
            )
            StashAPI.repository_cache = {}

            try:
                raw_repos = self.connection.repos.list()
            except NotFoundException as e:
                raise(StashError(e))

            for repo in raw_repos:
                links = {}
                for link in repo['links']['clone']:
                    links[link['name']] = link['href']

                rtmp = MODEL.Repo(
                        name=repo['name'],
                        description=repo['description'] if 'description' in repo else '',
                        path=repo['slug'] if 'slug' in repo else '',
                        url=repo['links']['self'][0]['href'],
                        ssh_clone_url=links['ssh'],
                        http_clone_url=links['http']
                    ).save()

                project = repo['project']['key']

                # If project was specified we update that
                if project is not None:
                    if project not in StashAPI.repository_cache:
                        StashAPI.repository_cache[project] = {}
                        StashAPI.repository_cache[project]['repos'] = []
                        StashAPI.repository_cache[project]['cache_timestamp'] \
                            = datetime.now()

                if rtmp in StashAPI.repository_cache[project]['repos']:
                    app.logger.warn(f'Duplicate entry {rtmp.name} found in response from stash for project {project}')

                StashAPI.repository_cache[project]['repos'].append(rtmp)

            StashAPI.repository_cache_timestamp = datetime.now()

        return StashAPI.repository_cache

    def project_repos(self, project, refresh=False):
        """Returns listing of SCM repositories within a project

        Arguments:
            project {str} -- Name of the project
            refresh {bool} -- If True force refresh of the cache

        Returns:
            list() -- List or repositories
        """

        # If forced refresh or first init or expired then we fetch
        if refresh or \
            project not in StashAPI.repository_cache or \
            (datetime.now() - StashAPI.repository_cache[project]
                ['cache_timestamp']).seconds > 3600:

            app.logger.debug(
                'Fetching {} repo listing from stash...'.format(project)
            )

            try:
                raw_repos = self.connection.projects[project.upper()].repos.list()
            except NotFoundException as e:
                raise(StashError(e))

            repos = []
            for repo in raw_repos:
                links = {}
                for link in repo['links']['clone']:
                    links[link['name']] = link['href']

                rtmp = MODEL.Repo(
                        name=repo['name'],
                        description=repo['description'] if 'description' in repo else '',
                        path=repo['slug'] if 'slug' in repo else '',
                        url=repo['links']['self'][0]['href'],
                        ssh_clone_url=links['ssh'],
                        http_clone_url=links['http']
                    ).save()

                repos.append(rtmp)

            # If project was specified we update that
            if project is not None:
                if project not in StashAPI.repository_cache:
                    StashAPI.repository_cache[project] = {}

                StashAPI.repository_cache[project]['repos'] = repos
                StashAPI.repository_cache[project]['cache_timestamp'] \
                    = datetime.now()

        return StashAPI.repository_cache[project]['repos']
