import os


def dir_size(path='.'):
    """Quick util script to scan a directory tree and sum
    the total size

    Keyword Arguments:
        path {str} -- Path to scan (default: {'.'})

    Returns:
        [int] -- Size of directory tree in bytes
    """

    total = 0
    for entry in os.scandir(path):
        if entry.is_file():
            total += entry.stat().st_size
        elif entry.is_dir():
            total += dir_size(entry.path)
    return total


def dir_tree(path='.'):
    """Quick util to graph a directory tree

    Keyword Arguments:
        path {str} -- [description] (default: {'.'})
    """

    tree = {path: []}
    for entry in os.scandir(path):
        if entry.is_file():
            tree[path].append(entry.path)
        elif entry.is_dir():
            tree[path].append(dir_tree(entry.path))
    return tree
