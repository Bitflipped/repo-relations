import inspect
import importlib

from repo_relations.app import app  # noqa


def get_parser(dep_type, language):
    """Generator to get an instance of a parser for dep_type

    Arguments:
        dep_type {str} -- Type of known dependency
    """

    try:
        stack_frame = inspect.stack()[1]

        app.logger.debug(f'get_parser stack_frame: {stack_frame}')

        base_mod = inspect.getmodule(stack_frame[0])

        app.logger.debug(f'get_parser base_mod: {base_mod}')

    except Exception as ex:
        app.logger.error(ex)
        raise ex

    try:
        package = base_mod.__package__

        app.logger.debug(f'get_parser package: {package}')

        parsers = importlib.import_module(f'{package}.{language}.parser')
        return getattr(parsers, f'_{dep_type.capitalize()}Parser')()
    except Exception as ex:
        app.logger.error(
            f'Unable to locate parser matching {dep_type}'
        )
        raise ex
