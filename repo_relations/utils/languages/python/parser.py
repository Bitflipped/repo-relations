import pip  # noqa

from pprint import pprint  # noqa
from repo_relations.app import app  # noqa

from repo_relations.models import get_model

MODEL = get_model()


class _PipParser(object):
    """PIP Composer parser
    """

    def parse(self, file_path):
        try:
            with open(file_path, 'r') as dep_fp:
                data = None

        except IOError as io_ex:
            app.logger.error(io_ex)
            raise io_ex

        return data

    def parse_requirements(data):
        pass

    def parse_setup_py(data):
        pass
