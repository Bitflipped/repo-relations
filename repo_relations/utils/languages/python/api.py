from pprint import pprint  # noqa
from repo_relations.app import app  # noqa

from repo_relations.utils.languages.api import BaseDeps


class Deps(BaseDeps):
    """Encapsulates the concept of a language dependency
    """

    LANGUAGE = "python"

    DEP_FILES = {
        'pip': [
            'requirements.txt',
            'requirements/*.txt'
        ],
        'setuptools': [
            'setup.py'
        ]
    }

    DEP_TOOLS = {
        'pip': 'pip',

    }
