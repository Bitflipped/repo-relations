import unittest
import tempfile
import os

from repo_relations.utils.languages.parsers import get_parser


class ParserTest(unittest.TestCase):
    def setUp(self):
        "Create mock composer file for parse testing"
        self.temp_file = tempfile.NamedTemporaryFile(delete=False)
        self.temp_filename = self.temp_file.name

        with open(self.temp_filename, 'w') as tmp:
            tmp.write("""
{
    "name": "test/fixture",
    "require": {
        "php/package"               : "1.*"
    },
    "require-dev": {
        "phpunit/phpunit"        : "*"
    }
}
""")

    def tearDown(self):
        "Cleanup mock composer file"
        self.temp_file.close()
        os.unlink(self.temp_filename)

    def test_get_parser_php_composer(self):
        parser = get_parser('composer', 'php')

        self.assertIsNotNone(parser)

    def test_php_parse(self):
        pass


if __name__ == '__main__':
    unittest.main()
