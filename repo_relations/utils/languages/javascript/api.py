from pprint import pprint  # noqa
from repo_relations.app import app  # noqa

from repo_relations.utils.languages.api import BaseDeps


class Deps(BaseDeps):
    """Encapsulates the concept of a language dependency
    """

    LANGUAGE = "javascript"

    DEP_FILES = {
        'yarn': [
            'package.json'
        ]
    }

    DEP_TOOLS = {
        'yarn': 'yarn'
    }

    DEP_FILE_TYPES = {
        'yarn': 'json'
    }
