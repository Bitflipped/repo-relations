"""Language base dependency collector utilities
"""

from repo_relations.app import app  # noqa

# Import language libs
from repo_relations.utils.languages.php.api import Deps as PHPApi  # noqa
from repo_relations.utils.languages.javascript.api import Deps as JSApi  # noqa
from repo_relations.utils.languages.python.api import Deps as PythonApi  # noqa


def get_api(base_path):
    """Return API object based on language passed in.

    Arguments:
        base_path {str} -- Base path to the source code to manage deps for.

    Returns:
        [type] -- [description]
    """

    return [
        PHPApi(base_path),
        JSApi(base_path),
        #PythonApi(base_path)
    ]
