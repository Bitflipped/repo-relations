from pprint import pprint  # noqa
from repo_relations.app import app  # noqa

from repo_relations.utils.languages.parsers import get_parser
from repo_relations.utils.languages.filesystem import locate_files


class BaseDeps(object):
    """Encapsulates the concept of a language dependency
    """

    def __init__(self, base_path):
        """Constructor

        Arguments:
            base_path {str} -- Repository base path to be used as root dir
        """

        self.base_path = base_path
        self.dep_files = locate_files(self.base_path, self.DEP_FILES)

        app.logger.debug(f'Creating {self.LANGUAGE} deps object.')

    def get_all(self):
        for dep_type, dep_files in self.dep_files.items():
            parser = get_parser(dep_type, self.LANGUAGE)
            for dep_file in dep_files:
                yield parser.parse(dep_file)
