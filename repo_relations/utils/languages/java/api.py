from pprint import pprint  # noqa
from repo_relations.app import app  # noqa

from repo_relations.utils.languages.api import BaseDeps


class Deps(BaseDeps):
    """Encapsulates the concept of a language dependency
    """

    LANGUAGE = "java"

    DEP_FILES = {
        'gradle': [
            'build.gradle'
        ]
    }

    DEP_TOOLS = {
        'gradle': 'gradle'
    }

    DEP_FILE_TYPES = {
        'gradle': 'build.gradle'
    }
