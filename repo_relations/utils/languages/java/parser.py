import json

from pprint import pprint  # noqa
from repo_relations.app import app  # noqa

from repo_relations.models.neo4j.dep import Dep

from repo_relations.models import get_model

MODEL = get_model()


class _GradleParser(object):
    """Gradle parser
    """

    DEP_KEYS = {
    }

    LANGUAGE = 'java'

    def parse(self, file_path):
        try:
            #  ./gradlew dependencies --configuration runtime --console=plain
            pass

        except:
            app.logger.error(f'Something broke!: No idea what...')
