"""Language base dependency collector filesystem utilities
"""

from pathlib import Path

from repo_relations.app import app  # noqa


def locate_files(base_path, dep_files):
    """Locate files within base_path that match patterns defined within the
    dep_files arg

    Arguments:
        base_path {str} -- Base path to search for dep files within
        dep_files {dict} -- Dep filetypes object
    """

    files = {}
    located_count = 0
    for dep_name, filetypes in dep_files.items():
        files[dep_name] = []
        for filetype in filetypes:
            p = Path(base_path)

            file_pattern = f'**/{filetype}'

            app.logger.debug(
                f'Searching {base_path} for {filetype}'
            )

            for file_path in [f for f in p.glob(file_pattern) if f.is_file()]:
                located_count += 1
                files[dep_name].append(str(file_path))

    app.logger.debug('Located {} dependency files in {}'.format(
        located_count,
        base_path
    ))

    return files
