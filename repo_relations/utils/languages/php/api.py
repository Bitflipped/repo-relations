from pprint import pprint  # noqa
from repo_relations.app import app  # noqa

from repo_relations.utils.languages.api import BaseDeps


class Deps(BaseDeps):
    """Encapsulates the concept of a language dependency
    """

    LANGUAGE = "php"

    DEP_FILES = {
        'composer': [
            'composer.json'
        ]
    }

    DEP_TOOLS = {
        'composer': 'composer'
    }
