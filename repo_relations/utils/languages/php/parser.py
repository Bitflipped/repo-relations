import json

from pprint import pprint  # noqa
from repo_relations.app import app  # noqa

from repo_relations.models.neo4j.dep import Dep

from repo_relations.models import get_model

MODEL = get_model()


class _ComposerParser(object):
    """PHP Composer parser
    """

    DEP_KEYS = {
        'require': 'runtime-deps',
        'require-dev': 'build-deps'
    }

    LANGUAGE = 'php'

    def parse(self, file_path):
        try:
            with open(file_path, 'r') as dep_fp:
                data = json.load(dep_fp)

                if 'name' in data:
                    dep_model = Dep(name=data['name'], language=self.LANGUAGE).save()
                    for dep_key, dep_type in self.DEP_KEYS.items():
                        if dep_key in data:
                            for dep_name, dep_version in data[dep_key].items():
                                if dep_type == 'runtime-deps':
                                    dep_model.cache_add_runtime_dep(dep_name, dep_version)
                                if dep_type == 'build-deps':
                                    dep_model.cache_add_build_dep(dep_name, dep_version)

                    return dep_model
                else:
                    return None
        except json.decoder.JSONDecodeError as jde:
            app.logger.error(f'JSON decode error: {jde}')
        except IOError as ioe:
            app.logger.error(f'I/O Error: {ioe}')
