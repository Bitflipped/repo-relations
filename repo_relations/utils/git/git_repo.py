import shutil
import tempfile

import git

from repo_relations.app import app
from repo_relations.utils.filesystem import dir_size


class GitRepo(object):
    """Basic Git repository management class
    """

    def __init__(self, project, git_url):
        """Init a new GitRepo and generate temp cache path.

        Arguments:
            project {str} -- Project name
            git_url {str} -- Git URL for remote repo
        """

        app.logger.debug('Creating GitRepo for {}'.format(git_url))

        self.project = project
        self.git_url = git_url

        self.repo_dir = tempfile.mkdtemp(dir=app.config['GIT_TEMP_PATH'])

        app.logger.info(
            'Created temporary repo cache path at {}'.format(self.repo_dir)
        )

    def __del__(self):
        app.logger.info('Removing temp repo cache')

        shutil.rmtree(self.repo_dir, ignore_errors=True)

        app.logger.debug('Destroying local GitRepo for {}'.format(self.git_url))

    def clone(self):
        """Clone a git repository to a

        Returns:
            tuple(str, int) -- Local repo path and size of the cloned
                directory structure
        """

        app.logger.info('Cloning {} to {}'.format(self.git_url, self.repo_dir))
        git.Repo.clone_from(self.git_url, self.repo_dir)

        repo_size = dir_size(self.repo_dir)
        app.logger.info('Size of cloned repository {}'.format(repo_size))

        return (self.repo_dir, repo_size)
