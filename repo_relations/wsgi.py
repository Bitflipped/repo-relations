import guard

from repo_relations.app import app as flask_app
from repo_relations import routes  # noqa

app = guard.ContentSecurityPolicy(flask_app.wsgi_app, report_only=True)
