from repo_relations.app import app  # noqa
from neomodel import config
config.DATABASE_URL = app.config['DB_HOST']
from repo_relations.models.neo4j.project import Project  # noqa
from repo_relations.models.neo4j.repo import Repo  # noqa

from repo_relations.models.neo4j.dep import Dep  # noqa
