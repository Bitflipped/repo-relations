from neomodel import (
    StructuredNode,
    StringProperty,
    RelationshipTo,
    RelationshipFrom
)

from repo_relations.app import app  # noqa


class Repo(StructuredNode):
    """Base model class for a repo concept
    """
    name = StringProperty(index=True)
    description = StringProperty()
    path = StringProperty()
    url = StringProperty(unique_index=True)
    ssh_clone_url = StringProperty()
    http_clone_url = StringProperty()

    project = RelationshipFrom('repo_relations.models.neo4j.project.Project', 'BELONGS_TO')
    dependencies = RelationshipTo('repo_relations.models.neo4j.dep.Dep', 'DEPENDS')
