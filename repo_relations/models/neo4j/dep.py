from pprint import pprint  # noqa

import neomodel

from neomodel import (
    StructuredNode,
    StringProperty,
    Relationship,
    RelationshipFrom,
    Traversal,
    cardinality
)

from repo_relations.app import app  # noqa


class Dep(StructuredNode):
    """Base model class for a repo concept
    """
    name = StringProperty(unique_index=True)
    language = StringProperty(index=True)

    build_dependency = Relationship('Dep', 'BUILD_DEPENDS_ON')
    runtime_dependency = Relationship('Dep', 'RUNTIME_DEPENDS_ON')

    repository = RelationshipFrom('repo_relations.models.neo4j.repo.Repo', 'DEPENDS', cardinality=cardinality.One)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.build_dep_cache = {}
        self.runtime_dep_cache = {}

    def link_cached(self):
        app.logger.debug(f'Attempting to locate and link cached deps for {self.name}')
        for build_dep_name, build_dep_version in self.build_dep_cache.items():
            app.logger.debug(f'Attempting to locate build dep {build_dep_name}')

            try:
                build_dep = Dep.nodes.get_or_none(name=build_dep_name)
            except neomodel.exceptions.MultipleNodesReturned as mn_ex:
                app.logger.error(
                    f'Build dependency locator for {build_dep_name} returned multiple graph nodes, skipping link phase duplicate entry whould be investigated.'
                )
                continue

            if build_dep is not None:
                app.logger.debug(f'Linking build dep {build_dep} for {self.repository[0].name}')
                # build_dep.build_dependency.connect(self)
                self.build_dependency.connect(build_dep)

        for runtime_dep_name, runtime_dep_version in self.runtime_dep_cache.items():
            app.logger.debug(f'Attempting to locate runtime dep {runtime_dep_name}')

            try:
                runtime_dep = Dep.nodes.get_or_none(name=runtime_dep_name)
            except neomodel.exceptions.MultipleNodesReturned as mn_ex:
                app.logger.error(
                    f'Runtime dependency locator for {runtime_dep_name} returned multiple graph nodes, skipping link phase duplicate entry whould be investigated.'  # noqa
                )
                continue

            if runtime_dep is not None:
                app.logger.debug(f'Linking runtime dep {runtime_dep} for {self.repository[0].name}')
                # runtime_dep.runtime_dependency.connect(self)
                self.runtime_dependency.connect(runtime_dep)

    def cache_add_build_dep(self, dep, metadata=None):
        app.logger.debug(f'Adding build dependency metadata for {dep} to {self.name}')
        self.build_dep_cache[dep] = metadata

    def cache_add_runtime_dep(self, dep, metadata=None):
        app.logger.debug(f'Adding runtime dependency metadata for {dep} to {self.name}')
        self.runtime_dep_cache[dep] = metadata

    def get_dependent_dependencies(self):
        """Get incoming depencies
        """

        definition = {
            'node_class': Dep,
            'direction': 'INCOMING',
            'relation_type': 'RUNTIME_DEPENDS_ON',
            'model': Dep
        }

        dependent_traversal = Traversal(self, Dep.__label__, definition)

        dependent_repos = list(set([d.repository.single().name for d in dependent_traversal.all()]))

        return dependent_repos
