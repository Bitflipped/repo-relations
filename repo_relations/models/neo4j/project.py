from neomodel import (
    StructuredNode,
    StringProperty,
    RelationshipTo
)

from repo_relations.app import app  # noqa


class Project(StructuredNode):
    """Base model class for a project concept
    """
    __primarykey__ = 'key'

    name = StringProperty(index=True)
    description = StringProperty()
    key = StringProperty(unique_index=True)

    repos = RelationshipTo('repo_relations.models.neo4j.repo.Repo', 'BELONGS_TO')
