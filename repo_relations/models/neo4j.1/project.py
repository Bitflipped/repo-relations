from py2neo.ogm import (
    GraphObject,
    Property,
    RelatedTo
)

from repo_relations.app import app  # noqa


class Project(GraphObject):
    """Base model class for a project concept
    """
    __primarykey__ = 'key'

    name = Property()
    description = Property()
    key = Property()

    repos = RelatedTo('repo_relations.models.neo4j.repo.Repo', 'HAS')

    def __init__(self, graph, name, description, key):
        """Constructor for a basic repo object

        Arguments:
            graph {py2neo.database.Graph} -- Graph database context
            name {str} -- Repo name
            description {str} -- Repo description
            key {str} -- Repo URI path
        """
        self.graph = graph
        self.name = name
        self.description = description
        self.key = key

        self.commit()

    def commit(self):
        self.graph.push(self)
