from py2neo.ogm import (
    GraphObject,
    Property,
    RelatedTo,
    RelatedFrom
)

from repo_relations.app import app  # noqa


class Repo(GraphObject):
    """Base model class for a repo concept
    """
    __primarykey__ = 'url'

    name = Property()
    description = Property()
    path = Property()
    url = Property()
    ssh_clone_url = Property()
    http_clone_url = Property()

    project = RelatedFrom('repo_relations.models.neo4j.project.Project', 'HAS')
    dependencies = RelatedTo('repo_relations.models.neo4j.dep.Dep', 'DEPENDENCIES')

    def __init__(
        self,
        graph,
        name,
        description,
        key,
        url,
        ssh_clone_url,
        http_clone_url
    ):
        """Constructor for a basic repo object

        Arguments:
            graph {py2neo.database.Graph} -- Graph database context
            name {str} -- Repo name
            description {str} -- Repo description
            key {str} -- Repo URI key
            url {str} -- Repo web URL
            ssh_clone_url {str} -- Repo ssh clone URL
            http_clone_url {str} -- Repo http(s) clone URL
        """
        self.graph = graph
        self.name = name
        self.description = description
        self.key = key
        self.url = url
        self.ssh_clone_url = ssh_clone_url
        self.http_clone_url = http_clone_url

        self.commit()

    def link_project(self, project):
        app.logger.debug(f'Linking {self.name} to project {project.name}')
        self.project.update(project)
        self.commit()

    def link_dependency(self, dependency):
        app.logger.debug(f'Adding {dependency.name} to know dependencies of {self.name}')
        self.dependencies.update(dependency)
        self.commit()

    def commit(self):
        self.graph.push(self)
