from py2neo.ogm import (
    GraphObject,
    Property,
    RelatedTo,
    RelatedFrom,
    Related
)

from repo_relations.app import app  # noqa


class Dep(GraphObject):
    """Base model class for a repo concept
    """
    __primarykey__ = 'name'

    name = Property()
    language = Property

    build_dependency = Related('Dep', 'BUILD_DEPENDS_ON')
    runtime_dependency = Related('Dep', 'RUNTIME_DEPENDS_ON')

    repository = RelatedFrom('repo_relations.models.neo4j.repo.Repo')

    def __init__(
        self,
        graph,
        name,
        language
    ):
        """Constructor for a basic repo object

        Arguments:
            graph {py2neo.database.Graph} -- Graph database context
            name {str} -- Dependency name
        """
        self.graph = graph
        self.name = name
        self.language = language
        self.build_dep_cache = {}
        self.runtime_dep_cache = {}

        self.commit()

    def link_cached(self):
        for build_dep_name, build_dep_version in self.build_dep_cache.items():
            app.logger.debug(f'Attempting to locate build dep {build_dep_name}')
            build_dep = Dep.select(self.graph, build_dep_name).first()

            if build_dep is not None:
                app.logger.debug(f'Located build dep {build_dep}')
                self.link_build_dependency(build_dep)

        for runtime_dep_name, runtime_dep_version in self.runtime_dep_cache.items():
            app.logger.debug(f'Attempting to locate runtime dep {runtime_dep_name}')
            runtime_dep = Dep.select(self.graph, runtime_dep_name).first()

            if runtime_dep is not None:
                app.logger.debug(f'Located runtime dep {runtime_dep}')
                self.link_runtime_dependency(runtime_dep)

    def link_repository(self, repository):
        app.logger.debug(f'Linking repo {repository} to {self.name}')
        self.repository.update(repository)
        self.commit()

    def link_build_dependency(self, dependency):
        app.logger.debug(f'Linking build dep {dependency} to {self.name}')
        self.build_dependency.update(dependency)
        self.commit

    def link_runtime_dependency(self, dependency):
        app.logger.debug(f'Linking runtime dep {dependency} to {self.name}')
        self.runtime_dependency.update(dependency)
        self.commit

    def commit(self):
        self.graph.push(self)

    def cache_add_build_dep(self, dep, metadata=None):
        app.logger.debug(f'Adding build dependency metadata for {dep} to {self.name}')
        self.build_dep_cache[dep] = metadata

    def cache_add_runtime_dep(self, dep, metadata=None):
        app.logger.debug(f'Adding runtime dependency metadata for {dep} to {self.name}')
        self.runtime_dep_cache[dep] = metadata
