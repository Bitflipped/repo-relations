from py2neo import Graph

from repo_relations.app import app


def get_connection():
    """Returns an instantiated Neo4J Graph connection

    Arguments:
        host {str} -- Host to connect to
        username {str} -- Username to connect as
        password {str} -- Password

    Returns:
        py2neo.Graph -- Graph context
    """

    return Graph(
        app.config['DB_HOST'],
        username=app.config['DB_USERNAME'],
        password=app.config['DB_PASSWORD']
    )


def locate_project(graph, project_key):
    """Attempts to locate graph object of a project

    Arguments:
        graph {py2neo.Graph} -- Graph DB connection
        project_key {str} -- Key of project to look for
    """
    from repo_relations.models.neo4j.project import Project

    Project.select(graph, project_key).first()
