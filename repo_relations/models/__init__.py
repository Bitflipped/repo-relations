from repo_relations.app import app

from repo_relations.models import neo4j


def get_model():
    """Return model module.

    Returns:
        [type] -- [description]
    """

    if app.config['DB_BACKEND'] == 'neo4j':
        return neo4j

    return None
