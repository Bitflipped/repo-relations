venv_dir=$(PWD)/venv
venv_python=$(venv_dir)/bin/python3
requirements_dir=$(PWD)/requirements

.PHONEY: check clean

clean: clean-pyc clean-build clean-requirements clean-venv

clean-pyc:
	@find . -name '__pycache__' -exec rm -rf {} +
	@find . -name '*.pyc' -exec rm -f {} +
	@find . -name '*.pyo' -exec rm -f {} +

clean-build:
	@rm -rf build/
	@rm -rf dist/
	@rm -rf *.egg-info

clean-requirements:
	@rm -f $(requirements_dir)/*.txt

clean-venv:
	@rm -rf $(venv_dir)

lint: dependencies
		$(venv_dir)/bin/pip-sync $(requirements_dir)/test.txt
		$(venv_dir)/bin/flake8 --doctests --show-source --count *.py repo_relations/

test: clean-pyc dependencies
		$(venv_dir)/bin/pip-sync  $(requirements_dir)/test.txt
		$(venv_dir)/bin/nosetests -c .noserc

quick-test: clean-pyc
		$(venv_dir)/bin/nosetests -c .noserc
coverage: dependencies
		$(venv_dir)/bin/pip-sync  $(requirements_dir)/test.txt
		$(venv_dir)/bin/coverage report -m

docker: check-docker dependencies test
	docker build -t repo-relations:latest .

run: dependencies
		$(venv_dir)/bin/pip-sync  $(requirements_dir)/base.txt
		$(venv_dir)/bin/gunicorn repo_relations.wsgi:app -w 2 -k gevent --access-logfile=- --error-logfile=-

$(requirements_dir): venv $(requirements_dir)/*.in  setup.py
	( \
		. $(venv_dir)/bin/activate; \
		cd $(requirements_dir); \
		make; \
	)

dependencies: venv $(requirements_dir)

venv:
	export PYTHON_EXECUTABLE=${which python3}
	test -d $(venv_dir) || virtualenv -p python3 $(venv_dir)
	$(venv_dir)/bin/pip install pip-tools
	@test -f $(venv_python) > /dev/null

check:
	@which python3 > /dev/null

check-docker:
	@which docker > /dev/null
