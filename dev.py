#!/usr/bin/env python

from repo_relations.app import app
from repo_relations import routes  # noqa

app.run(debug=True)
