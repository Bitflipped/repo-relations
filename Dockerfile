FROM python:3

LABEL maintainer="Greg Horejsi <greg.horejsi@kasasa.com>"

# Seed app user
RUN groupadd -g 999 app && \
    useradd -m -d /app -r -u 999 -g app app

# Python system setup setup
RUN pip install --upgrade pip && \
    pip install pip-tools gunicorn && \
    apt-get update && \
    apt-get install -y git  && \
    apt-get autoremove -y && \
    apt-get purge -y $(dpkg --list |grep '^rc' |awk '{print $2}') && \
    rm -rf /var/lib/apt-cache

WORKDIR /app

COPY --chown=app:app . /app

RUN pip-sync requirements/base.txt

USER app

CMD ["gunicorn", "--workers=2", "-k gevent", "--access-logfile=-", "--error-logfile=-", "--bind=0.0.0.0:5000", "repo_relations.wsgi:app"]
