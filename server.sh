#!/bin/bash

VENV_DIR=$PWD/venv
REQUIREMENTS_DIR=$PWD/requirements

$VENV_DIR/bin/pip-sync  $REQUIREMENTS_DIR/base.txt
$VENV_DIR/bin/gunicorn repo_relations.wsgi:app -w 2 -k gevent --access-logfile=- --error-logfile=-
